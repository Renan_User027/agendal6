<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProprietariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 70);
            $table->string('rg', 12);
            $table->string('cpf', 14);
            $table->Integer('idade');
            $table->char("sexo", 12);
            $table->date("data_nasc");
            $table->string("endereco", 100);
            $table->string("telefone", 10);
            $table->string("celular", 13);
            $table->string("email", 100);
            $table->string("facebook", 100);
            $table->string("twitter", 100);
            $table->string("instagram", 100);
            $table->string("orkut", 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proprietarios');
    }
}
