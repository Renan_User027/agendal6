@extends('adminlte::page')

@section('title', 'Agenda')

@section('content_header')
<h1><i class = "fas fa-fx fa-bars">Lista de Agendas</i></h1>
@stop

@section('content')
    <div class = "panel panel-default">
        <div class = "panel-heading clearfix">
            Relação dos Contatos da Agenda
            <div class = "pull-right">
                <a href="#" class= "btn btn-primary btn-sm"><i class = "fas fa-fx fa-sync-alt"></i>Atualizar a Tela</a>
                <a href="#" class= "btn btn-success btn-sm"><i class = "fas fa-fx fa-plus"></i>Adicionar novo contato</a>
            </div>
        </div>

        <div class = "panel-body">
            <table id="tabela" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID </th>
                        <th>NOME</th>
                        <th>CELULAR</th>
                        <th>EMAIL</th>
                        <th>AÇÕES</th>

                    </tr>
                </thead>

                <tbody>
                    @foreach($contatos as $contato)
                    <tr>
                        <td>{{  $contato->id    }}</td>
                        <td>{{  $contato->nome  }}</td>
                        <td>{{  $contato->fone_res  }}</td>
                        <td>{{  $contato->fone_cel  }}</td>
                        <td>{{  $contato->dt_nasc   }}</td>
                        <td>{{  $contato->email     }}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>                            
        </div>

        <div class = "panel-footer">
            
        </div>
    </div>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $(document).ready( function () {
    $('#tabela').DataTable();
    } );    
</script>
@stop